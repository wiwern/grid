import { Component } from '@angular/core';
import items from '../../../items.json';
import { ProductsService } from '../services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent {
  games = items;
  constructor(private productsService: ProductsService) { }

  //adding to cart subject
  public addGameToCart(game: any): void {
    this.productsService.addProduct(game)
  }

}
