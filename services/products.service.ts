import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  public addProductSubject = new Subject();

  constructor() { }

  public addProduct(product: any): void {
    this.addProductSubject.next(product);
  }
}
