export class Game {
  id: number;
  title: string;
  cover: string;
  amount: number;
  quantity: number;
  price: number;
  currency: string;

  constructor(id: number,
    title: string,
    cover: string,
    amount: number,
    quantity: number,
    price: number,
    currency: string) {

      this.id = id;
    this.title = title;
    this.cover = cover;
    this.amount = amount;
    this.quantity = quantity;
    this.price = price;
    this.currency = currency

  }

}
