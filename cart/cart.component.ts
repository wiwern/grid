import { Component, OnInit } from '@angular/core';
import { ProductsService } from '../services/products.service';
import { Game } from './Game';


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {
  public total: number = 0;
  public cartWithProducts: any[] = []

  constructor(private productsService: ProductsService) { }

  ngOnInit(): void {
    this.productsService.addProductSubject.subscribe( (game: any) => {
      let quantity = 1;

      //searching for same element in array
    for (const product of this.cartWithProducts) {
      if (product.id === game.id) {
        quantity = product.quantity + 1;
        this.cartWithProducts.splice(this.cartWithProducts.indexOf(product), 1);
      }
    }
     //setting new product and add to cart
    let amount = quantity * game.price;
    let gameInCart = new Game(
      game.id,
      game.title,
      game.cover,
      amount,
      quantity, game.price, game.currency);
    this.cartWithProducts.push(gameInCart);
    this.total = 0

      //setting total
    this.cartWithProducts.map (x => {
      this.total = x.amount +  this.total;
    });


    });
  }

}
